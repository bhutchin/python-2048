import os
import random


class GameLauncher:
    def __init__(self):
        self.board = [
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0]
        ]
        self.win = False
        self.lose = False

    def insert_in_random_location(self, insert_count):
        blank_spaces = self.find_all_blank_spots()
        spaces_to_insert = random.choices(blank_spaces, k=insert_count)
        for space in spaces_to_insert:
            self.board[space[0]][space[1]] = random.choices([2, 4], weights=[4, 1])[0]


    def init_game(self):
        self.insert_in_random_location(2)


    def find_all_blank_spots(self):
        blank_spaces = []
        for row_number, row in enumerate(self.board):
            for column_number, space in enumerate(row):
                if space == 0:
                    coordinates = (row_number, column_number)
                    blank_spaces.append(coordinates)
        return blank_spaces


    def print_board(self):
        for row in self.board:
            print(row)


    def move_right(self):
        moved_board = []
        for row_number, row in enumerate(self.board):
            row.reverse()
            for column_number, digit in enumerate(row):
                if digit == 0:
                    row.remove(row[column_number])
                    row.append(0)
            moved_board.append(row)
        merged_board = []
        for row_number, row in enumerate(moved_board):
            for column_number, digit in enumerate(row):
                if column_number > 0 and digit > 0:
                    if row[column_number] == row[column_number - 1]:
                        row[column_number - 1] = row[column_number - 1] * 2
                        row[column_number] = 0
            row.reverse()
            merged_board.append(row)
        self.board = merged_board


    def move_left(self):
        moved_board = []
        for row_number, row in enumerate(self.board):
            for column_number, digit in enumerate(row):
                if digit == 0:
                    row.remove(row[column_number])
                    row.append(0)
                if column_number > 0:
                    if row[column_number] == row[column_number - 1]:
                        row[column_number - 1] == row[column_number - 1] * 2
                        row[column_number] == 0
            moved_board.append(row)
        merged_board = []
        for row_number, row in enumerate(moved_board):
            for column_number, digit in enumerate(row):
                if column_number > 0 and digit > 0:
                    if row[column_number] == row[column_number - 1]:
                        row[column_number - 1] = row[column_number - 1] * 2
                        row[column_number] = 0
            merged_board.append(row)
        self.board = merged_board


    def move_up(self):
        column_board = []
        for i in range(0, 4):
            column = []
            for row in self.board:
                column.append(row[i])
            column_board.append(column)
        self.board = column_board
        self.move_left()
        return_board = []
        for i in range(0, 4):
            column = []
            for row in self.board:
                column.append(row[i])
            return_board.append(column)
        self.board = return_board


    def move_down(self):
        column_board = []
        for i in range(0, 4):
            column = []
            for row in self.board:
                column.append(row[i])
            column_board.append(column)
        ##This might be hacky to update the instance to a temp instance
        self.board = column_board
        self.move_right()
        return_board = []
        for i in range(0, 4):
            column = []
            for row in self.board:
                column.append(row[i])
            return_board.append(column)
        self.board = return_board


    def make_move(self):
        no_move = True
        while no_move:
            direction = input()
            if direction == 'd':
                self.move_right()
            if direction == 'a':
                self.move_left()
            if direction == 'w':
                self.move_up()
            if direction == 's':
                self.move_down()
            self.insert_in_random_location(1)
            return self.board


    def run_game(self):
        game.init_game()
        os.system('cls' if os.name == 'nt' else 'clear')
        self.print_board()
        end_game = True
        while end_game:
            self.board = self.make_move()
            os.system('cls' if os.name == 'nt' else 'clear')
            self.print_board()
            blank_spots = self.find_all_blank_spots()
            if not blank_spots:
                print('YOU LOSE')
                end_game = False


    def check_win(self):
        for row in self.board:
            for digit in row:
                if digit == 2048:
                    self.win = True
        return self.win

    def check_lose(self):
        if not self.find_all_blank_spots():
            self.lose = True
        return self.lose

if __name__ == "__main__":
    game = GameLauncher()
    game.run_game()
