
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QHBoxLayout, QGroupBox, QDialog, QVBoxLayout, QGridLayout, QLabel, QFrame, QMessageBox
from PyQt5.QtGui import QIcon, QFont
from PyQt5.QtCore import pyqtSlot, Qt

import main

## TODO machine learning setup to autoplay the game
## TODO scale background colours based on tile value
## TODO fix tiles not moving in 2 in front are merged
## TODO fix tiles being added on key press even if board hasnt been chagned
## TODO animate movement of

class App(QDialog):

    def __init__(self):
        super().__init__()
        self.title = 'Termi48'
        self.left = 50
        self.top = 50
        self.width = 320
        self.height = 320
        self.initUI()
        
        
    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        windowLayout = QVBoxLayout()
        self.horizontalGroupBox = QGroupBox("2048")
        windowLayout.addWidget(self.horizontalGroupBox)
        self.layout = QGridLayout()
        self.setLayout(windowLayout)
        self.home_screen()
        self.show()
        self.start.clicked.connect(self.update_board)


    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape:
            self.close()
        if event.key() == Qt.Key_Up:
            board.move_up()
            board.insert_in_random_location(1)
            self.update_board()
        elif event.key() == Qt.Key_Down:
            board.move_down()
            board.insert_in_random_location(1)
            self.update_board()
        elif event.key() == Qt.Key_Left:
            board.move_left()
            board.insert_in_random_location(1)
            self.update_board()
        elif event.key() == Qt.Key_Right:
            board.move_right()
            board.insert_in_random_location(1)
            self.update_board()
        

        if board.check_win():
            self.win_screen()
        if board.check_lose():
            self.lose_screen()


    def update_board(self):
        
        for i in reversed(range(self.layout.count())): 
            self.layout.itemAt(i).widget().deleteLater()
        
        row_count = 4
        column_count = 4
        for row_number, row in enumerate(board.board, start=1):
            font = QFont('Futura', 20, 20)
            for column_number, digit in enumerate(row, start=1):
                if digit == 0:
                    digit = ''
                    rgb = '255,255,255'
                else:
                    color = 255 / digit
                    print(color)
                    rgb = '255, {color}, 99'.format(color=str(color))
                tile = QLabel(str(digit))
                tile.setStyleSheet("background-color:rgb({rgb});".format(rgb=rgb))
                tile.setFont(font)
                tile.setFrameShape(QFrame.Panel)
                tile.setFrameShadow(QFrame.Sunken)
                tile.setAlignment(Qt.AlignCenter)
                self.layout.addWidget(tile, row_number, column_number, 1, 1)
        
        self.horizontalGroupBox.setLayout(self.layout)


    def win_screen(self):
        win = QMessageBox()
        win.setWindowTitle('You Won!')
        win.setText("You Won!")
        win.exec_()


    def lose_screen(self):
        lose = QMessageBox()
        lose.setWindowTitle('You lost')
        lose.setText("You lost, You suck!")
        lose.exec_()


    def home_screen(self):
        
        font = QFont('Futura', 60, 20)   
        title = QLabel('2048')
        title.setAlignment(Qt.AlignCenter)
        title.setFont(font)
        auto_solve = QPushButton()
        auto_solve.setText("Solve")
        auto_solve.resize(4, 1)
        self.start = QPushButton()
        self.start.setText("Start")
        end_game = QPushButton()
        end_game.setText("Quit")
        self.layout.addWidget(title, 1, 1, 2, 4)
        self.layout.addWidget(self.start, 3, 3, 1, 2)
        self.layout.addWidget(end_game, 3, 1, 1, 2)
        self.layout.addWidget(auto_solve, 4, 1, 1, 4)
        self.horizontalGroupBox.setLayout(self.layout)


if __name__ == '__main__':
    board = main.GameLauncher()
    board.init_game()
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
